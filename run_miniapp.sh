#!/bin/bash

# set parameters to run mimiapp aplication
N_TRD=$1  # first argument
M_SIZE=$2 # second argument
B_SIZE=$3 # third argument

echo
echo "Running miniapp with the following option set:"
echo "   -> number of threads: ${N_TRD}"
echo "   -> matrix size: ${M_SIZE}"
echo "   -> block size: ${B_SIZE}"
echo "   -> started ..."
echo

# load needed modules
module purge
module load foss
module load CMake
module load Boost

# My local paths
rpath=$(pwd)
TM=../sequential

LAPACKPP_PATH=${rpath}/${TM}/lapackpp
BLASPP_PATH=${rpath}/${TM}/blaspp
HPX_PATH=${rpath}/${TM}/hpx
DLAF_PATH=${rpath}/${TM}/dlaf
MINI_APP=${rpath}/dlai-unilj_fs

source /opt/intel/compilers_and_libraries/linux/mkl/bin/mklvars.sh intel64
export blaspp_DIR=${BLASPP_PATH}
export lapackpp_DIR=${LAPACKPP_PATH}
export HPX_DIR=${HPX_PATH}

export OMP_NUM_THREADS=1

${MINI_APP}/build/miniapp/cholesky_factorization_test_dlaf \
	--hpx:threads=${N_TRD} --matrix-size=${M_SIZE} --block-size=${B_SIZE} --check-result=last
