#!/bin/bash
#
# author: aleksander.grm@fpp.uni-lj.si
# date: 25/08/2020

# ******************************************************************
# *** Install & test DLA-Interface system locally in specified path ***
# ******************************************************************

# Needed modules/paths to be loaded
#
export MKLROOT=/usr

# Parameters
#
rpath=$(pwd) # set local path for the installation
nproc=32     # number of processors for parallel system build

# Clean previous installations
#
echo
echo " *** Cleaning old instalations ..."
./clean_all.sh
read -t 2 -p " Done with cleaning!"
echo

echo
echo "*** DLA-Interface system build directory: ${rpath}"
echo


# *** Build DLA-Interface
echo
read -t 2 -p " Starting with DLA-Interface ..."
echo
cd ${rpath}
./install_dlai.sh ${rpath} ${nproc}
echo
read -t 2 -p " Done with DLA-Interface!"
echo
