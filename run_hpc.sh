#!/bin/bash
#SBATCH --partition=haswell
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --ntasks-per-node=1
#SBATCH --job-name=miniapp_test

# number of threads per node
N_TRD=$1  # first argument, number of threads
M_SIZE=$2 # second argument, matrix size
B_SIZE=$3 # third argument, block size
rpath=$(pwd)

${rpath}/run_miniapp.sh ${N_TRD} ${M_SIZE} ${B_SIZE}
